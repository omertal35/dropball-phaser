import 'phaser';
//import {BaseGame} from '../interfaces/base-game.interface';
import { EmbedArts } from './../configuration/EmbedArt';
import * as GameEvents from '../constants/events.const';

export class BootScene extends Phaser.Scene {


    constructor() {
        super({ key: 'BootScene' });
        console.log('BootScene')
    }
    init() {
        console.log('init');
    }
    preload(): void {

        console.log('preload');

        let logo_64 = EmbedArts.logo;
        this.textures.addBase64("logo", logo_64);

        let width = this.cameras.main.width;
        let height = this.cameras.main.height;
        let progressBoxData = {
            width: width * 0.60,
            height: height * 0.05,
            x: 0,
            y: 0
        }
        progressBoxData.x = (width - progressBoxData.width) * 0.50;
        progressBoxData.y = (height - progressBoxData.height) * 0.50 - progressBoxData.height;

        let tW = progressBoxData.width * 0.90;
        let tH = progressBoxData.width - tW;
        let progressBarData = {
            width: tW,
            height: tH,
            x: progressBoxData.x + (progressBoxData.width - tW) * 0.50,
            y: progressBoxData.y + (progressBoxData.height - tH) * 0.50,
        }
        let progressBar = this.add.graphics();
        let progressBox = this.add.graphics();

        //PROGRESS BOX
        progressBox.fillStyle(0x222222, 0.8);
        progressBox.fillRect(progressBoxData.x, progressBoxData.y, progressBoxData.width, progressBoxData.height);

        //progressBox.setPosition(, height*0.50);
        //progressBox.setPosition((width-320)*0.50,0);

        // var loadingText = this.make.text({
        //     x: width / 2,
        //     y: height / 2 - 50,
        //     text: 'Loading...',
        //     style: {
        //         font: '20px monospace'
        //         //fill: '#ffffff'
        //     }
        // });
        // loadingText.setOrigin(0.5, 0.5);

        // var percentText = this.make.text({
        //     x: width / 2,
        //     y: height / 2 - 5,
        //     text: '0%',
        //     style: {
        //         font: '18px monospace'
        //         //fill: '#ffffff'
        //     }
        // });
        // percentText.setOrigin(0.5, 0.5);

        // var assetText = this.make.text({
        //     x: width / 2,
        //     y: height / 2 + 50,
        //     text: '',
        //     style: {
        //         font: '18px monospace'
        //         //fill: '#ffffff'
        //     }
        // });
        // assetText.setOrigin(0.5, 0.5);


        this.load.on('progress', function (value: any) {
            //var str = (value*100).toString();
            //percentText.setText(parseInt(str) + '%');
            progressBar.clear();
            progressBar.fillStyle(0xffffff, 1);
            progressBar.fillRect(progressBarData.x, progressBarData.y, progressBarData.width * value, progressBarData.height);
        });

        // this.load.on('fileprogress', function (file:any) {
        //     assetText.setText('Loading asset: ' + file.key);
        // });


        this.load.on('complete', function () {
            //.log('boot complete')
            //console.log(progressBar);
            progressBar.destroy();
            progressBox.destroy();
            // loadingText.destroy();
            // percentText.destroy();
            // assetText.destroy();
            //this.scene.start('TitleScene');
        });

        //this.load.image('wiply', 'assets/ui/wiply_small_logo.png');
        // var logo = this.add.image(
        //     0,
        //     0,
        //     "wiply"
        // )
        // logo.setPosition(720/2,1240/2);
        this.time.addEvent({ delay: 50, callback: this.showLogo.bind(this), loop: false });
        //console.log("should show asset list");
        let data = this.cache.json.get('assets_list');
        //console.log(JSON.stringify(data));
        //var display_list = data.display_list;
        // for(var i = 0; i<display_list.length; i++)
        // {
        //     console.log(display_list[i].key);
        //     this.load.image(display_list[i].key, display_list[i].path);
        // }
        //Object.keys(data).forEach(e => console.log(`key=${e}  value=${data[e].path}`));

        // var phaserGame:PhaserGame = this.game as PhaserGame;
        // var coupons = phaserGame.localGame.gameOptions.coupons;
        // //console.log("Boot scene: " + JSON.stringify(coupons));
        // for(var i=0; i<=coupons.length; i++)
        // {
        //     if(typeof(coupons[i])!=='undefined' && coupons[i] != null)
        //     {
        //         //console.log(coupons[i]);
        //         this.load.image('coupon'+coupons[i].id, coupons[i].imgUrl);
        //     }

        // }

        Object.keys(data).forEach(e => {

            //console.log("key: " + e);
            //console.log("group: " + data[e].group);

            if (data[e].group == "display") {
                this.load.image(e, data[e].display_path);
            }
            else if (data[e].group == "atlas") {
                this.load.atlas(e, data[e].display_path, data[e].data_path);
            }
            else if (data[e].group == "font") {
                //console.log("font key: " + e);
                this.load.bitmapFont(e, data[e].display_path, data[e].data_path);
            }
            else if (data[e].group == "data") {
                //console.log(e + " path: " + data[e].data_path)
                this.load.json(e, data[e].data_path)
            }
            else if (data[e].group == "audio") {
                //console.log(e + " path: " + data[e].data_path)
                this.load.audio(e, data[e].data_path)
            }

        });


    }

    create(): void {

        this.setupAnimations();
        this.scene.start('GameScene');
        this.game.events.emit(GameEvents.GAME_LOADED, 'game loaded');
    }

    setupAnimations() {
        //ANIMATIONS
        this.anims.create({
            key: 'playbutton',
            frameRate: 20,
            frames: this.anims.generateFrameNames("gametitle_textureatlas", {
                prefix: "game_title_",
                suffix: ".png",
                start: 0,
                end: 9
            }),
            repeat: -1
        });

        this.anims.create({
            key: 'drone_release_',
            frameRate: 20,
            frames: this.anims.generateFrameNames("drone_textureatlas", {
                prefix: "drone_drop_",
                suffix: ".png",
                start: 1,
                end: 16
            }),
            repeat: 0
        });

        this.anims.create({
            key: 'drone_idle_',
            frameRate: 20,
            frames: this.anims.generateFrameNames("drone_textureatlas", {
                prefix: "drone_idle_",
                suffix: ".png",
                start: 1,
                end: 16
            }),
            repeat: 1
        });

        this.anims.create({
            key: 'star_rotate',
            frameRate: 24,
            frames: this.anims.generateFrameNames("star_textureatlas", {
                prefix: "star_animation_",
                suffix: ".png",
                start: 0,
                end: 15
            }),
            repeat: -1
        });
    }

    showLogo() {
        let logo = this.add.image(
            0,
            0,
            "logo"
        )
        logo.setPosition(720 / 2, 1240 / 2);
    }


}