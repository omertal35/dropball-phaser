import { BaseGame } from "../../interfaces/base-game.interface";

type Constructor<T = {}> = new (...args: any[]) => T;

export function applyMixins(derivedCtor: any, baseCtors: any[]) {
    baseCtors.forEach(baseCtor => {
        Object.getOwnPropertyNames(baseCtor.prototype).forEach(name => {
            let descriptor = Object.getOwnPropertyDescriptor(baseCtor.prototype, name)
            Object.defineProperty(derivedCtor.prototype, name, <PropertyDescriptor & ThisType<any>>descriptor);
        });
    });
}

export function applyClassMixin<TBase extends Constructor>(Base: TBase) {
    return class extends Base {
    };
}



export class InteractiveObject extends BaseGame {

    constructor() {
        super();
    }

    public onGameLoaded() {
        console.log("InteractiveObject: onGameLoaded")
    };

    public onGameLost() {

    };
    public onGameEnd() {

    };
    public onGameWon(couponId: string) {

    };

    public retryGame() {

    };

    public dispose?() {

    };
}
