//import { ISpriteConstructor } from '../interfaces/sprite.interface';
import { WiplySpriteInterface } from '../interfaces/wiplysprite.interface';

export class Redhat extends Phaser.GameObjects.Sprite {

  //body: Phaser.Types.Physics.Matter.MatterBody;
  local_color:string;

  constructor(aParams: WiplySpriteInterface) {
    
    super(aParams.scene, aParams.x, aParams.y, aParams.texture, aParams.frame);

    if(aParams.color!=null) this.local_color = aParams.color;
   
    this.initSprite();
    this.initPhysics();
    this.scene.add.existing(this);
  }

  protected initSprite() {
    this.setScale(0.5);
  }

  protected initPhysics() {
    //this.scene.physics.world.enable(this);
    //this.body.setVelocity(100, 200);
    //this.body.setBounce(1, 1);
    //this.body.setCollideWorldBounds(true);
  }
}
