export const GAME_ALIVE = 'GAME_ALIVE';
export const GAME_WON_EVENT = 'GAME_WON_EVENT';
export const GAME_OVER = 'GAME_OVER';
export const GAME_LOADED = 'GAME_LOADED';
