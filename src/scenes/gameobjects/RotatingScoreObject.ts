import { LocalGameConfig } from '../../configuration/GameConfig';
import { WiplySpriteInterface } from '../../interfaces/wiplysprite.interface';
import { ScoreObjects } from './ScoreObjects';

export class RotatingScoreObject extends ScoreObjects {

    

    constructor(aParams: WiplySpriteInterface) {

        super(aParams);

        if (aParams.color != null) this.local_color = aParams.color;

        this.initSprite();
        this.initPhysics();
        this.scene.add.existing(this);
    }

    protected initSprite() {

    }

    protected initPhysics() {
        
    }

    public update() {
        if (this.kineticSubMovementType == LocalGameConfig.SUB_MOVEMENT_TYPE_ROTATING) this.rotation += this.localRotation;
    }


}
