import { Redhat } from '../objects/redhat';
import { LocalGameConfig } from '../configuration/GameConfig';
import { IGameOptions } from './../interfaces/base-game.interface';
import * as GameEvents from '../constants/events.const';

export class MainScene extends Phaser.Scene {

  private myRedhat: Redhat;
  private colorTheme: any;
  private globalConfig: any;
  private hasLogoImgUrl: boolean = false;

  constructor() {
    super({ key: 'MainScene' });
    console.log('MainScene');

  }

  init() {
    console.log('init MainScene!')
    this.game.events.emit(GameEvents.GAME_ALIVE, 'MyCouponID');
  }

  preload(): void {
    console.log('preload MainScene')
    this.load.json('assets_list', 'assets/assets.json');

    this.continueRegistry();
  }

  continueRegistry() {
    this.globalConfig = this.registry.get('globals');

    if (this.globalConfig.logoImgUrl !== null && typeof (this.globalConfig.logoImgUrl) !== 'undefined') {
      this.hasLogoImgUrl = true;
      this.load.image(this.globalConfig.logoFilename, this.globalConfig.logoImgUrl);
    }
  }

  

  create(): void {

    
    this.scene.start('BootScene')


  }

  setOptions(options: IGameOptions) {

    let coupons = options.coupons;
    console.log('coupons------');


    let globalConfig = {
      'appWidth': 720,
      'appHeight': 1240,
      'center': { 'x': (720 / 2), 'y': 1240 / 2 },
      'centerOffset': { 'x': (720 / 2), 'y': 1240 / 2 },
      colorTheme: {},
      coupons,
      logoImgUrl: "",
      logoFilename: ""
    };



    globalConfig.logoImgUrl = options.logoImgUrl;
    let fileName = this.split(globalConfig.logoImgUrl);
    console.log("filename: " + fileName);
    globalConfig.logoFilename = fileName;
    this.setColorTheme(options.colors);

    globalConfig.colorTheme = this.colorTheme;
    this.registry.set('globals', globalConfig);


  }

  split(str: string) {
    return str.split('\\').pop().split('/').pop();
  }


  replace(str: string, target: string, replaceWith: string) {
    return str.replace(target, replaceWith);
  }
  setColorTheme(colors: Array<any>) {
    let temp: Array<string> = [];
    for (let i: integer = 0; i < colors.length; i++) {
      temp.push(this.replace(colors[i], '#', '0x'));
    }

    colors = temp;

    let c: any;

    if (LocalGameConfig.COLOR_THEME_ON) {
      c = {
        background: [colors[0], colors[1], colors[0]],
        bounds: colors[1],
        obstacles: colors[1],
        rotators: [colors[1], colors[1]],
        leftScoreWall: colors[1],
        rightScoreWall: colors[1],
        movingPlatform: colors[1],
        hero: colors[0],
        pickups: colors[2],
        lifter: colors[2],
        goals: [colors[2], colors[2], colors[2]],
        diamonds: [colors[1], colors[1], colors[1], colors[1], colors[1]],
        goalsFinished: [colors[2], colors[2], colors[2]],
        gameTitle: colors[2]
      };
    }
    else {
      //NOTES: no colors 0xffffff (white) : DEFAULT IS WHITE 0XFFFFFF
      c = {
        background: [0xffffff, 0xffffff, 0x0000ff],
        bounds: 0xffffff,
        obstacles: 0xffffff,
        rotators: [0xffffff, 0xffffff],
        leftScoreWall: 0xffffff,
        rightScoreWall: 0xffffff,
        movingPlatform: 0xffffff,
        hero: 0xffffff,
        pickups: 0xffffff,
        lifter: 0xffffff,
        goals: [0xffffff, 0xffffff, 0xffffff],
        diamonds: [0xffffff, 0xffffff, 0xffffff, 0xffffff, 0xffffff],
        goalsFinished: [0xffffff, 0xffffff, 0xffffff],
        gameTitle: 0xffffff
      };
    }

    this.colorTheme = c;

  }
  configureGlobalColors() {

  }
}
