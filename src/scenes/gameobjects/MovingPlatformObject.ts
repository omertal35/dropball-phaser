import { WiplySpriteInterface } from '../../interfaces/wiplysprite.interface';
//import * as GameEnums from '../../../src/configuration/GameEnums';
import { ScoreObjects } from './ScoreObjects';

export class MovingPlatformObject extends ScoreObjects {

    constructor(aParams: WiplySpriteInterface) {

        super(aParams);

    }

    public initialize() {
        this.origX = this.x;
        this.leftBounds = this.origX - this.maxDistance;
        this.rightBounds = this.origX + this.maxDistance;
        if (this.direction == 0) this.direction = 1;
    }
    protected initSprite() {

    }

    protected initPhysics() {

    }

    public update() {

        //console.log("moving platform" + this.direction);
        //this.rotation += 0.90;
        //this.x += this.speed;
        

        if (this.direction > 0) {
            
            if (this.x > this.rightBounds) {
                //this.x = this.origX + this.maxDistance;
                this.direction = -1;
            }
            else
            {
                this.x += this.speed;
            }
        }
        else {
            
            if (this.x < this.leftBounds) {
                //this.x = this.origX - this.maxDistance;
                this.direction = 1;
            }
            else
            {
                this.x -= this.speed;
            }
        }
    }

}
