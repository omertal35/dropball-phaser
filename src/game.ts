import 'phaser';
import { GameConfig } from './config';
import { BaseGame, IGameCoupon, IGameOptions } from "./interfaces/base-game.interface";
import {MainScene} from './scenes/main-scene';
import * as GameEvents from "./constants/events.const";

let game;

export class PhaserGame extends Phaser.Game {

  localGame: Game;
  name: string;

  constructor(config: Phaser.Types.Core.GameConfig) {
    super(config);
    console.log('PhaserGame');
    
  }
  setOptions(options: IGameOptions) {

    console.log('set options');
    console.log('mainscene: ' + (this.scene.getScene('MainScene') as MainScene).toString());
    let mainScene:MainScene = this.scene.getScene('MainScene') as MainScene;
    mainScene.setOptions(options);
    
  }
  getScenes() {
    console.log(this.scene);
  }
  setReference(game: Game) {
    this.localGame = game;
  }

}

export class Game extends BaseGame {
  coupons: IGameCoupon[]
  colors: string[]
  gameOptions: IGameOptions;

  constructor(
    private phaserGame: PhaserGame,
    options?: IGameOptions
  ) {
   
    super();
    console.log('Game');
    this.initEvents();
    this.gameOptions = options;
    //this.phaserGame.setOptions(options);

  }

  onGameLost() { 
    console.log("GAME LOST!");
  };
  onGameEnd() { 

    console.log("GAME ENDED!")
    // createGame();
  };
  onGameWon(couponId: string) {

    //send to something
    console.log("GAME WON:: couponId: " + couponId);

   };

  initEvents() {
    
    this.phaserGame.events.on(GameEvents.GAME_WON_EVENT, (couponId: string) => {
      this.onGameWon(couponId)
    });
    this.phaserGame.events.on(GameEvents.GAME_ALIVE, (couponId: string) => {
      this.phaserGame.setOptions(this.gameOptions);
    })
    this.phaserGame.events.on(GameEvents.GAME_OVER, (couponId: string) => {
      this.onGameEnd();
    })
    this.phaserGame.events.on(GameEvents.GAME_LOADED, (couponId: string) => {
      this.onGameLoaded();
    })
  }

  setCoupons(coupons: IGameCoupon[]) {
    super.setCoupons(coupons);
    this.coupons = coupons || []
    // TODO: Replace coupons in game.
  }

  setColors(colors: string[]) {
    super.setColors(colors);
    this.colors = colors || []
    // TODO: Paint game with new colors.
  }

  getGame(): PhaserGame {
    return this.phaserGame;
  }
  check() {
    console.log("checking!!!");
  }
}

function createGame()
{
  console.log('create game');
  let options: IGameOptions = {
    logoImgUrl: 'https://res.cloudinary.com/shulgirit/image/upload/v1614781634/wiply/Asset_1_nw8s9t.png',
    colors: ['#ffc0cb', '#e0e0e0', '#effd5f'],//['0x29A0B1', '0x167D7F', '0x98D7C2'],
    coupons: [
      { id: "1", "imgUrl": "https://res.cloudinary.com/shulgirit/image/upload/v1614784879/wiply/Webp.net-resizeimage_2_eu8qgn.png" },
      { id: "2", "imgUrl": "https://res.cloudinary.com/shulgirit/image/upload/v1614785009/wiply/Webp.net-resizeimage_3_lplvur.png" },
      { id: "3", "imgUrl": "https://res.cloudinary.com/shulgirit/image/upload/v1614785013/wiply/Webp.net-resizeimage_4_zpy5zn.png" }]
  };

  game = new Game(new PhaserGame(GameConfig), options);
  // game.gameOptions = options;
  // phaserGame.setReference(game);

  //console.log(game.getGame().getScenes());

  window.addEventListener('resize', onResize)
  onResize();
}
window.addEventListener('load', () => {

  createGame();
  // let phaserGame = new PhaserGame(GameConfig);
  // phaserGame.name = "wiply";
  
});

function onResize() {

  let doc = document.getElementById("game");
  let descendants = doc.getElementsByTagName('canvas');
  let canvas = descendants[0]

  let width = window.innerWidth, height = window.innerHeight;
  let wratio = width / height, ratio = canvas.width / canvas.height;

  if (wratio < ratio) {
    canvas.style.width = width + "px";
    canvas.style.height = (width / ratio) + "px";
  } else {
    canvas.style.width = (height * ratio) + "px";
    canvas.style.height = height + "px";
  }
}
