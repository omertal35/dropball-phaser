import 'phaser';
import { StaticObjects } from './gameobjects/StaticObjects'
import {IWorldColor} from '../interfaces/base-game.interface';
//import { Game, PhaserGame } from './../game';
//import { GameConfig } from './../config';

export class TitleScene extends Phaser.Scene {

    playBtn: Phaser.GameObjects.Sprite;
    wiply: Phaser.GameObjects.Sprite;
    backgroundSound:any;
    colorTheme:IWorldColor;
    globalConfig:any;
    hasLogoImgUrl:Boolean = false;
    logo:Phaser.GameObjects.Image;
    
    constructor() {
        super({ key: 'TitleScene' });
    }

    preload(): void {

        this.globalConfig = this.registry.get('globals');

        if(this.globalConfig.logoImgUrl !== null && typeof(this.globalConfig.logoImgUrl)!=='undefined')
        {
            this.hasLogoImgUrl = true;
            this.load.image(this.globalConfig.logoFilename, this.globalConfig.logoImgUrl);
        }
       //this.load.atlas('game_title', 'assets/game_title/titlescreen_sheet.png', 'assets/game_title/titlescreen_sheet.json');
    }

    create(): void {

        
        
        // var phaserGame:PhaserGame = this.game as PhaserGame;
        // var coupons = phaserGame.localGame.gameOptions.coupons;
        // globalConfig.coupons = coupons;
        // this.registry.set('globals', globalConfig);

        this.colorTheme = this.globalConfig.colorTheme;

        
        
        //this.colorTheme = this.registry.get('globals').colorTheme;
        
        this.backgroundSound = this.sound.add("Drop Ball SC1");
        this.backgroundSound.play({loop:true});
        
        //this.add.text(0, 0, "Title Screen!");

        //ANIMATIONS
        this.anims.create({
            key: 'playbutton',
            frameRate: 20,
            frames: this.anims.generateFrameNames("gametitle_textureatlas", {
                prefix: "game_title_",
                suffix: ".png",
                start: 0,
                end: 9
            }),
            repeat: -1
        });

        this.anims.create({
            key: 'drone_release_',
            frameRate: 20,
            frames: this.anims.generateFrameNames("drone_textureatlas", {
                prefix: "drone_drop_",
                suffix: ".png",
                start: 1,
                end: 16
            }),
            repeat: 0
        });

        this.anims.create({
            key: 'drone_idle_',
            frameRate: 20,
            frames: this.anims.generateFrameNames("drone_textureatlas", {
                prefix: "drone_idle_",
                suffix: ".png",
                start: 1,
                end: 16
            }),
            repeat: 1
        });

        this.anims.create({
            key: 'star_rotate',
            frameRate: 24,
            frames: this.anims.generateFrameNames("star_textureatlas", {
                prefix: "star_animation_",
                suffix: ".png",
                start: 0,
                end: 15
            }),
            repeat: -1
        });

        


        let bg = new StaticObjects({
            scene: this,
            x: this.registry.get('globals').centerOffset.x,
            y: this.registry.get('globals').centerOffset.y,
            texture: 'bg_flat'
        });
        bg.setOrigin(0,0);
        bg.setPosition(0,0);
        bg.setInteractive();

        let overlay = new StaticObjects({
            scene: this,
            x: this.registry.get('globals').centerOffset.x,
            y: this.registry.get('globals').centerOffset.y,
            texture: 'bg_silhoette'
        });
        overlay.setOrigin(0,0);
        overlay.setPosition(0,0);
        overlay.setInteractive();

        bg.setTint(this.colorTheme.background[0]);
        overlay.setTint(this.colorTheme.background[2]);
       
        bg.alpha = 0;
        overlay.alpha = 0;

        this.playBtn = this.add.sprite(
            this.registry.get('globals').centerOffset.x,
            this.registry.get('globals').centerOffset.y,
            "title_textureatlas"
        );
        this.playBtn.play("playbutton");
        //this.playBtn.setInteractive(true);
        this.playBtn.setTint(this.colorTheme.gameTitle);

        let offsetX = this.playBtn.width*0.50;
        let offsetY = this.playBtn.height;

        this.wiply = this.add.sprite(
            this.registry.get('globals').centerOffset.x + offsetX,
            this.registry.get('globals').centerOffset.y + offsetY,
            "logo"
        );
        this.wiply.setPosition(
            this.registry.get('globals').centerOffset.x + offsetX - this.wiply.width*0.50, 
            this.registry.get('globals').centerOffset.y + offsetY)

        //this.input.enabled = true;
        // var w = this.registry.get('globals').appWidth;
        // var h = this.registry.get('globals').appHeight;
        // var r1 = this.add.rectangle(w*0.50, h*0.50, w, h, 0x6666ff);
        // r1.alpha = 0;
        // this.input.setHitArea(r1);
        // this.input.on('gameobjectdown', this.onClick, this);

        let x = this.globalConfig.appWidth * 0.50;
        let y = this.playBtn.y;// + this.playBtn.height*0.50;//this.globalConfig.appHeight * 0.60 + this.playBtn.height*0.30;
        let scale = 0.50;
        this.logo = this.add.sprite(x, y, this.globalConfig.logoFilename);
        this.logo.scale = scale;
        this.logo.setPosition(x, y + this.logo.height*0.50);
            
        if(this.hasLogoImgUrl)
        {
            this.wiply.alpha = 0;
        }

        this.input.on('pointerdown', () =>this.onClick());
        
        
        //this.add.sprite(200,200,'coupon1');
    }

    onClick(): void {
        console.log('did clicked title screen');
        this.scene.start('GameScene')
    }
}