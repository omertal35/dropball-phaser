export class LocalGameConfig
{
    static GAME_TYPE_HERO:integer = 0;
    static GAME_TYPE_BOUNDS:integer = 1;
    static GAME_TYPE_INTERACTIVE_BOUNDS:integer = 2;
    static GAME_TYPE_ENEMY:integer = 3;
    static GAME_TYPE_PICKUP:integer = 4;

    static MOVEMENT_TYPE_NONE:integer = -1;
    static MOVEMENT_TYPE_STEADY:integer = 0;
    static MOVEMENT_TYPE_HORIZONTAL:integer = 1;
    static MOVEMENT_TYPE_VERTICAL:integer = 2;
    static MOVEMENT_TYPE_DIAGONAL:integer = 3;
    
    static SUB_MOVEMENT_TYPE_STEADY_NONE:integer = -1;
    static SUB_MOVEMENT_TYPE_STEADY:integer = 0;
    static SUB_MOVEMENT_TYPE_ROTATING:integer = 1;
    static SUB_MOVEMENT_TYPE_JUMPING:integer = 2;

    static COLOR_THEME_ON:boolean = true;
}