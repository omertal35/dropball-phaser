import { WiplySpriteInterface } from '../../interfaces/wiplysprite.interface';
//import * as GameEnums from '../../../src/configuration/GameEnums';

export class ScoreObjects extends Phaser.GameObjects.Sprite {

    //body: Phaser.Types.Physics.Matter.MatterBody;
    local_color: string;
    gameObjectType:integer;
    kineticMovementType:integer;
    kineticSubMovementType:integer;
    localRotation:number = 0;
    
    //getter and setters
    _leftBounds:number = 0;
    _rightBounds:number = 0;
    _maxDistance:number = 0;
    _direction:integer = 0;
    _origX:number = 0;
    _origY:number = 0;
    _speed:number = 2;
    _isInitialized:boolean = false;

    constructor(aParams: WiplySpriteInterface) {

        super(aParams.scene, aParams.x, aParams.y, aParams.texture, aParams.frame);

        if (aParams.color != null) this.local_color = aParams.color;

        this.initSprite();
        this.initPhysics();
        this.scene.add.existing(this);
    }

    public initialize()
    {

    }

    protected initSprite() {

    }

    protected initPhysics() {
        
    }

    public update() {

    }

    /* getter and setters */
    public get speed() {
        return this._speed;
    }

    public set speed(value: number) {
        this._speed = value;
    }
    public get maxDistance() {
        return this._maxDistance;
    }

    public set maxDistance(value: number) {
        this._maxDistance = value;
    }

    public get leftBounds() {
        return this._leftBounds;
    }

    public set leftBounds(value: number) {
        this._leftBounds = value;
    }

    public get rightBounds() {
        return this._rightBounds;
    }

    public set rightBounds(value: number) {
        this._rightBounds = value;
    }

    public get origX() {
        return this._origX;
    }

    public set origX(value: number) {
        this._origX = value;
    }

    public get origY() {
        return this._origY;
    }

    public set origY(value: number) {
        this._origY = value;
    }

    public get direction() {
        return this._direction;
    }

    public set direction(value: number) {
        this._direction = value;
    }
    
    public get isInitialized() {
        return this._isInitialized;
    }

    public set isInitialized(value: boolean) {
        this._isInitialized = value;
    }


}
