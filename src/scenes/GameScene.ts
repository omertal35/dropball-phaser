import 'phaser';
//import { Game, PhaserGame } from './../game';
//import { GameConfig } from './../config';
import { GameObjects } from 'phaser';
import { StaticObjects } from './gameobjects/StaticObjects';
//import { applyClassMixin } from './gameobjects/InteractiveObject';
//import { BaseGame } from '../interfaces/base-game.interface';
import { ScoreObjects } from './gameobjects/ScoreObjects';
//import * as GameEnums from '../configuration/GameEnums';
import { RotatingScoreObject } from './gameobjects/RotatingScoreObject';
import { LocalGameConfig } from '../configuration/GameConfig';
//import { Vector } from 'matter';
import { DroneObject } from './gameobjects/DroneObject';
import { MovingPlatformObject } from './gameobjects/MovingPlatformObject';
import { IWorldColor } from '../interfaces/base-game.interface';
import * as GameEvents from '../constants/events.const';
//import { IGameCoupon, IGameOptions } from '../game';

export class GameScene extends Phaser.Scene {

    //containers
    gameContainer: any;
    elementsContainer: any;
    ballContainer: any;
    particleContainer: any;
    overlayContainer: Phaser.GameObjects.Container;
    effectsContainer: any;
    companyLogoContainer:any;

    //Phaser game objects
    discountText: GameObjects.BitmapText;
    currentStar: StaticObjects;
    currentWinningCoupon: StaticObjects;
    ball: ScoreObjects;
    drone: StaticObjects;
    discountPanel: StaticObjects;
    ballBody: MatterJS.BodyType;
    globalConfig:any;
    logo:Phaser.GameObjects.Image;

    //Object
    specsObject: any;
    lifterGroupObject: any;
    staticGroupObject: any;
    backgroundGroupObject: any;
    heroGroupObject: any;
    scoringGroupObject: any;
    shapesObject: any;
    ballShapeObject: any;
    starGroupObject: any;
    goalGroupObject: any;
    otherGroupObject: any;
    gameTimeout: any;
    finalDiscountText: Phaser.GameObjects.BitmapText;

    colorTheme: IWorldColor;

    //Array
    starPositions: Array<any> = [];
    goals: Array<any> = [];
    finishedCoupons: Array<any> = [];
    movingList: Array<ScoreObjects> = [];
    activeTextPool: Array<Phaser.GameObjects.BitmapText> = [];
    reserveTextPool: Array<Phaser.GameObjects.BitmapText> = [];
    couponList: Array<any> = null;

    //integers
    ballRadius: integer = 28;
    w: integer = 0;
    h: integer = 0;
    releaseBall: integer = 0;
    goalDiscount: integer = 0;
    didReachGoal: integer = 0;

    //numbers
    gameDiscount: number = 0.0;
    discountGoal: number = 0.0;
    discountRotator: number = 0.5;
    discountGems: number = 0.2;
    discountYellowBar: number = 0.8;
    discountRedBar: number = 1.0;
    discountMovingPanel: number = 0.80;
    discountStar: number = 1.5;
    finalCount: number = 0.0;

    //PARTICLES
    emitter: any;
    pointEmitter: any;
    emitZone: any;
    pointParticles: any;

    //GROUPS
    scoreTextGroup: Phaser.GameObjects.Group

    //KEYS
    sKey: string;
    isColliding: boolean = false;
    canCountDiscount: boolean = false;
    hasLogoImgUrl:boolean = false;

    //audio
    bonusClang: any;
    hit: any;
    movingHit: any;
    pickup: any;
    wallHit: any;
    win: any;
    rotatorHit: any;
    shootOnCan: any;
    coin: any;
    backgroundSound: any;

    //wiply game
    //wGame: Game;
    currentCouponName: string;
    currentCouponId:string;
    currentCouponIndex:integer;
    couponMap:any;

    constructor() {
        super({ key: 'GameScene' });
    }

    preload(): void {
        //this.load.image('background', 'assets/ui/Sc1-BG.png');
        //this.load.image('wiply', 'assets/ui/Wiply-Small-Logo.png');
        //this.load.atlas('game_title', 'assets/Game Title/titlescreen_sheet.png', 'assets/Game Title/titlescreen_sheet.json');
        //this.load.plugin('rexglowfilterpipelineplugin', 'https://raw.githubusercontent.com/rexrainbow/phaser3-rex-notes/master/dist/rexglowfilterpipelineplugin.min.js', true);

        //var reg = this.registry.get('globals');
        this.globalConfig = this.registry.get('globals');
        let coupons = this.globalConfig.coupons;
        
        if(this.globalConfig.logoImgUrl !== null && typeof(this.globalConfig.logoImgUrl)!=='undefined')
        {
            this.hasLogoImgUrl = true;
            this.load.image(this.globalConfig.logoFilename, this.globalConfig.logoImgUrl);
        }

        for(let i=0; i<=coupons.length; i++)
        {

            if(typeof(coupons[i])!=='undefined' && coupons[i] != null)
            {
                coupons[i].index = i;
                this.load.image('coupon'+coupons[i].id, coupons[i].imgUrl);
            }
        }
    }

    create(): void {
        try {
             this.backgroundSound = this.sound.add("Drop Ball SC1");
             this.backgroundSound.play({loop:true});
        } catch (error) {
            
        }
      

        this.bonusClang = this.sound.add("bonus_clang");
        this.hit = this.sound.add("hit1");
        this.movingHit = this.sound.add("movinghit");
        this.pickup = this.sound.add("pickup");
        this.wallHit = this.sound.add("wallhit");
        this.win = this.sound.add("win1");
        this.rotatorHit = this.sound.add("rotatorhit");
        this.shootOnCan = this.sound.add("shoot_on_can");
        this.coin = this.sound.add("coin");

        this.colorTheme = this.registry.get('globals').colorTheme;
        this.w = this.registry.get('globals').appWidth;
        this.h = this.registry.get('globals').appHeight;

        this.specsObject = this.cache.json.get('specs')

        this.shapesObject = this.cache.json.get('dropball_physics_shapes');
        //console.log(this.shapesObject);

        //CONTAINER ASSEMBLY
        this.gameContainer = this.add.container(0, 0);
        this.companyLogoContainer = this.add.container(0,0);
        this.elementsContainer = this.add.container(0, 0);
        this.ballContainer = this.add.container(0, 0);
        this.particleContainer = this.add.container(0, 0);
        this.overlayContainer = this.add.container(0, 0);
        this.effectsContainer = this.add.container(0, 0);
        this.gameContainer.add(this.companyLogoContainer);
        this.gameContainer.add(this.elementsContainer);
        this.gameContainer.add(this.particleContainer);
        this.gameContainer.add(this.ballContainer);
        this.gameContainer.add(this.effectsContainer);
        this.gameContainer.add(this.overlayContainer);

        this.matter.world.setBounds(0, 0, this.w, this.h);
        // const rect = this.add.rectangle(this.w*0.50,this.h*0.50, this.w, this.h, 0x000000, 0.70);
        // this.overlayContainer.add(rect);
        // this.overlayContainer.alpha = 0;
        // this.overlayContainer.visible = false;

        //STAGE ASSEMBLY
        //this.initBackgrounds();
        this.initGameOverOverlay();
        this.initBoundaries();
        this.initHittables();
        this.initStar();
        this.initLifterAndPlayer();
        this.initCoupons();
        this.initScoreData();
        this.initCompanyLogo();
        this.connectPhysics();

        this.input.on(Phaser.Input.Events.POINTER_DOWN, (pointer: any) => {
            //this.spawnText(pointer.x, pointer.y, 'myContent');
            this.onClick();
        })
    }

    initBackgrounds() {

        this.backgroundGroupObject = this.specsObject.backgrounds;
        //console.log(this.backgroundGroupObject.bg_0.x);

        let bg1 = new StaticObjects({
            scene: this,
            x: this.backgroundGroupObject.bg_0.x * this.w,
            y: this.backgroundGroupObject.bg_0.y * this.h,
            texture: this.backgroundGroupObject.bg_0.texturename
        });
        bg1.name = this.backgroundGroupObject.bg_0.texturename;
        bg1.setOrigin(0, 0);
        bg1.setPosition(0, 0);

        let bg2 = new StaticObjects({
            scene: this,
            x: this.backgroundGroupObject.stardust.x * this.w,
            y: this.backgroundGroupObject.stardust.y * this.h,
            texture: this.backgroundGroupObject.stardust.texturename
        });
        bg2.name = this.backgroundGroupObject.stardust.texturename;
        bg2.setOrigin(0, 0);
        bg2.setPosition(0, 0);

        let bg3 = new StaticObjects({
            scene: this,
            x: this.backgroundGroupObject.bg_1.x * this.w,
            y: this.backgroundGroupObject.bg_1.y * this.h,
            texture: this.backgroundGroupObject.bg_1.texturename
        });
        bg3.name = this.backgroundGroupObject.bg_1.texturename;
        bg3.setOrigin(0, 0);
        bg3.setPosition(0, 0);
        bg1.setInteractive();

        bg1.setTint(this.colorTheme.background[0]);
        bg2.setTint(this.colorTheme.background[1]);
        bg3.setTint(this.colorTheme.background[2]);
        //this.input.on('gameobjectdown', this.onClick, this);

        this.input.on(Phaser.Input.Events.POINTER_DOWN, (pointer: any) => {
            //this.spawnText(pointer.x, pointer.y, 'myContent');
            this.onClick();
        })

        this.elementsContainer.add(bg1);
        this.elementsContainer.add(bg2);
        this.elementsContainer.add(bg3);


    }

    initGameOverOverlay() {
        //overlay
        let overlay = new StaticObjects({
            scene: this,
            x: 0,
            y: 0,
            texture: "sc_tint"
        });
        overlay.name = "sc_tint";
        overlay.setOrigin(0, 0);

        this.overlayContainer.add(overlay);


        const congrats = this.add.bitmapText(0, 0, 'font_export', '', 80);
        congrats.setOrigin(0.50, 0.50);
        congrats.text = "CONGRATULATIONS!";
        congrats.x = (this.w * 0.50);
        congrats.y = (this.h * 0.30);
        congrats.name = 'congrats';
        this.overlayContainer.add(congrats);
        congrats.alpha = 0;

        // const score = this.add.bitmapText(0, 0, 'font_export', '', 150);
        // score.setOrigin(0.50, 0.50);
        // score.text = "95%";
        // score.x = this.w * 0.50;
        // score.y = (congrats.y + congrats.height + score.height * 0.40);
        // score.name = 'score';
        // this.overlayContainer.add(score);
        // score.alpha = 0;


        // const discount = this.add.bitmapText(0, 0, 'font_export', '', 60);
        // discount.setOrigin(0.50, 0.50);
        // discount.text = "DISCOUNT!";
        // discount.x = this.w * 0.50;
        // discount.y = (score.y + score.height + score.height * 0.20);
        // discount.name = 'discount';
        // this.overlayContainer.add(discount);
        // discount.alpha = 0;

        this.overlayContainer.alpha = 0;
        this.overlayContainer.setVisible(false);
    }
    initBoundaries() {

        this.staticGroupObject = this.specsObject.static_bounds;

        //console.log("static objects: " + JSON.stringify(this.staticGroupObject));
        Object.keys(this.staticGroupObject).forEach(e => {

            if (this.staticGroupObject[e].texturename != "left_boundary" &&
                this.staticGroupObject[e].texturename != "right_boundary" &&
                this.staticGroupObject[e].texturename != "bottom_boundary" &&
                this.staticGroupObject[e].texturename != "boundary_up") {

                let entity = new StaticObjects({
                    scene: this,
                    x: this.staticGroupObject[e].x * this.w,
                    y: this.staticGroupObject[e].y * this.h,
                    texture: this.staticGroupObject[e].texturename
                });
                this.matter.add.gameObject(entity, { friction: 0, restitution: 1.1, x: entity.x, y: entity.y, isStatic: true, shape: this.shapesObject[this.staticGroupObject[e].texturename] });
                entity.setDepth(this.staticGroupObject[e].index);
                entity.name = this.staticGroupObject[e].texturename;
                // if (this.staticGroupObject[e].texturename == "left_boundary") {
                //     entity.setPosition(entity.x - entity.width * 0.25, entity.y);
                // }
                // if (this.staticGroupObject[e].texturename == "right_boundary") {
                //     entity.setPosition(this.w - entity.width * .24, entity.y);
                // }
                this.elementsContainer.add(entity);
                entity.setTint(this.colorTheme.bounds);
            }


        });



    }

    initHittables() {
        this.scoringGroupObject = this.specsObject.scoring_bounds;

        let diamondCount = 0;
        Object.keys(this.scoringGroupObject).forEach(e => {

            //console.log(e);

            switch (e) {
                case "moving_platform":

                    let movingPlatform = new MovingPlatformObject({
                        scene: this,
                        x: this.scoringGroupObject[e].x * this.w,
                        y: this.scoringGroupObject[e].y * this.h,
                        texture: this.scoringGroupObject[e].texturename
                    });
                    movingPlatform.setDepth(this.scoringGroupObject[e].index);
                    this.matter.add.gameObject(movingPlatform, { isStatic: true, frictionAir: 0, friction: 0.1, restitution: 1, x: movingPlatform.x, y: movingPlatform.y, shape: this.shapesObject[this.scoringGroupObject[e].texturename] });
                    movingPlatform.maxDistance = 50;
                    movingPlatform.direction = 1;
                    movingPlatform.speed = 3.9;
                    movingPlatform.initialize();
                    movingPlatform.name = this.scoringGroupObject[e].texturename;
                    this.movingList.push(movingPlatform);
                    this.elementsContainer.add(movingPlatform);
                    movingPlatform.setTint(this.colorTheme.movingPlatform);
                    break;

                case "hittable_0": case "hittable_1": case "hittable_2": case "hittable_3": case "hittable_4":
                case "rotator_0": case "rotator_1":
                    let rObj = new RotatingScoreObject({
                        scene: this,
                        x: this.scoringGroupObject[e].x * this.w,
                        y: this.scoringGroupObject[e].y * this.h,
                        texture: this.scoringGroupObject[e].texturename
                    });
                    rObj.gameObjectType = LocalGameConfig.GAME_TYPE_INTERACTIVE_BOUNDS;
                    rObj.kineticMovementType = LocalGameConfig.MOVEMENT_TYPE_STEADY;
                    rObj.kineticSubMovementType = LocalGameConfig.SUB_MOVEMENT_TYPE_ROTATING;
                    rObj.setDepth(this.scoringGroupObject[e].index);
                    this.movingList.push(rObj);

                    let rObjIndex = parseInt(e.substr(e.length - 1, 1));
                    rObj.localRotation = rObjIndex % 2 ? -0.05 : 0.05;
                    if (e.indexOf("rotator") >= 0) {
                        rObj.localRotation *= 2;
                        rObj.name = "rotator" + (rObjIndex % 2 ? 1 : 2);
                        rObjIndex % 2 ? rObj.setTint(this.colorTheme.rotators[0]) : rObj.setTint(this.colorTheme.rotators[1]);
                    }
                    else {
                        rObj.name = this.scoringGroupObject[e].texturename;
                        rObj.setTint(this.colorTheme.diamonds[rObjIndex])
                        diamondCount++;
                    }
                    this.matter.add.gameObject(rObj, { isStatic: true, frictionAir: 0, friction: 0.1, restitution: 1, x: rObj.x, y: rObj.y, shape: this.shapesObject[this.scoringGroupObject[e].texturename] });
                    this.elementsContainer.add(rObj);
                    break;

                case "hittable_5": case "hittable_6":
                    let sObj = new ScoreObjects({
                        scene: this,
                        x: this.scoringGroupObject[e].x * this.w,
                        y: this.scoringGroupObject[e].y * this.h,
                        texture: this.scoringGroupObject[e].texturename
                    });
                    sObj.gameObjectType = LocalGameConfig.GAME_TYPE_INTERACTIVE_BOUNDS;
                    sObj.kineticMovementType = LocalGameConfig.MOVEMENT_TYPE_STEADY;
                    sObj.kineticSubMovementType = LocalGameConfig.SUB_MOVEMENT_TYPE_STEADY;
                    sObj.setDepth(this.scoringGroupObject[e].index);
                    this.movingList.push(sObj);
                    sObj.name = this.scoringGroupObject[e].texturename;

                    this.matter.add.gameObject(sObj, { isStatic: true, frictionAir: 0, friction: 0.01, restitution: 1, x: sObj.x, y: sObj.y, shape: this.shapesObject[this.scoringGroupObject[e].texturename] });
                    this.elementsContainer.add(sObj);
                    let sObjIndex = parseInt(e.substr(e.length - 1, 1));
                    sObjIndex % 2 ? sObj.setTint(this.colorTheme.leftScoreWall) : sObj.setTint(this.colorTheme.rightScoreWall);
                    break;

                default:
            }



        });
    }

    initStar() {

        this.starGroupObject = this.specsObject.pickup_positions;
        Object.keys(this.starGroupObject).forEach(e => {
            this.starPositions.push(this.starGroupObject[e]);
        });
        //console.log(this.starPositions);
        let index = Math.floor(Math.random() * this.starPositions.length);

        this.currentStar = new StaticObjects({
            scene: this,
            x: this.starPositions[index].x * this.w,
            y: this.starPositions[index].y * this.h,
            texture: this.starPositions[index].texturename
        });
        this.currentStar.name = "star";
        this.currentStar.play("star_rotate");
        this.currentStar.setTint(this.colorTheme.pickups);

        this.matter.add.gameObject(this.currentStar,
            {
                isStatic: true,
                isSensor: true,
                x: this.currentStar.x,
                y: this.currentStar.y,
                shape: this.shapesObject["star_animation_0"]
            });

        this.currentStar.setDepth(this.starPositions[index].index);
        this.ballContainer.add(this.currentStar);
    }

    initLifterAndPlayer() {
        this.lifterGroupObject = this.specsObject.lifter;

        this.drone = new DroneObject({
            scene: this,
            x: this.lifterGroupObject.lifter.x * this.w,
            y: this.lifterGroupObject.lifter.y * this.h,
            texture: this.lifterGroupObject.lifter.texturename
        });
        this.drone.play({ key: 'drone_idle_', repeat: -1 });
        this.drone.setDepth(this.lifterGroupObject.lifter.index);
        this.drone.setTint(this.colorTheme.lifter);

        this.ball = new ScoreObjects({
            scene: this,
            x: this.lifterGroupObject.ball.x * this.w,
            y: this.lifterGroupObject.ball.y * this.h,
            texture: this.lifterGroupObject.ball.texturename
        });
        this.ball.name = "ball";
        this.ball.gameObjectType = LocalGameConfig.GAME_TYPE_HERO;
        this.ball.kineticMovementType = LocalGameConfig.SUB_MOVEMENT_TYPE_STEADY_NONE;
        this.ball.kineticSubMovementType = LocalGameConfig.SUB_MOVEMENT_TYPE_STEADY_NONE;
        this.ball.setDepth(this.lifterGroupObject.ball.index);
        this.movingList.push(this.ball);
        this.ball.setTint(this.colorTheme.hero);
        (this.drone as DroneObject).initialize();

        this.ballContainer.add(this.drone);
        this.ballContainer.add(this.ball);
    }

    initCoupons() {



        this.couponList = this.registry.get('globals').coupons;
        this.goalGroupObject = this.specsObject.goal_bounds;
        this.couponMap = {};

        let count: integer = 0;
        let scale: number = 0.50;

        Object.keys(this.goalGroupObject).forEach(e => {

            let x: number = this.goalGroupObject[e].x * this.w;
            let y: number = this.goalGroupObject[e].y * this.h;

            let coupon = this.matter.add.image(x, y, 'coupon' + this.couponList[count].id, null, { isStatic: true, isSensor: true }).setScale(scale, scale);
            this.couponMap['coupon' + this.couponList[count].id] = this.couponList[count];
            coupon.name = 'coupon' + this.couponList[count].id;
            this.goals.push(coupon);
            //coupon.setTint(this.colorTheme.goalsFinished[count]);

            coupon.setDepth(this.goalGroupObject[e].index);

            let couponFinished = this.add.sprite(x, y, 'coupon' + this.couponList[count].id).setScale(scale, scale);
            couponFinished.name = coupon.name + 'fin';
            couponFinished.setDepth(this.goalGroupObject[e].index + 1);
            couponFinished.alpha = 0;
            //couponFinished.setTint(this.colorTheme.goalsFinished[count]);

            this.finishedCoupons.push(couponFinished);
            this.elementsContainer.add(coupon);
            this.elementsContainer.add(couponFinished);
            count++;
        });

        // this.goalGroupObject = this.specsObject.goal_bounds;
        // Object.keys(this.goalGroupObject).forEach(e => {

        //     let goalIndex = parseInt(e.substr(e.length - 1, 1));

        //     let goal = new StaticObjects({
        //         scene: this,
        //         x: this.goalGroupObject[e].x * this.w,
        //         y: this.goalGroupObject[e].y * this.h,
        //         texture: this.goalGroupObject[e].texturename
        //     });
        //     goal.name = this.goalGroupObject[e].texturename;
        //     this.goals.push(goal);

        //     goal.setTint(this.colorTheme.goals[goalIndex]);
        //     goal.setDepth(this.goalGroupObject[e].index);

        //     this.matter.add.gameObject(goal, {
        //         isStatic: true,
        //         isSensor: true,
        //         shape: this.shapesObject[this.goalGroupObject[e].texturename],
        //         label: "ticket"
        //     });

        //     let gName = this.goalGroupObject[e].texturename.substr(0, this.goalGroupObject[e].texturename.length - 1);
        //     gName += "b";
        //     //console.log(gName);

        //     let goalFin = new StaticObjects({
        //         scene: this,
        //         x: this.goalGroupObject[e].x * this.w,
        //         y: this.goalGroupObject[e].y * this.h,
        //         texture: gName
        //     });
        //     goalFin.name = gName;
        //     goalFin.setDepth(this.goalGroupObject[e].index + 1);
        //     goalFin.alpha = 0;
        //     goalFin.setTint(this.colorTheme.goalsFinished[goalIndex]);

        //     this.finishedCoupons.push(goalFin);
        //     this.elementsContainer.add(goal);
        //     this.elementsContainer.add(goalFin);


        // });

    }


    initParticles() {
        let particles = this.add.particles('pixie_particle_0');
        this.emitZone = new Phaser.Geom.Circle(0, 0, this.ball.width * 0.10);

        let particleConfig: Phaser.Types.GameObjects.Particles.ParticleEmitterConfig = {
            speed: this.ball.body.velocity.y,
            emitZone: { type: 'random', source: this.emitZone },
            angle: { min: 0, max: 360 },
            scale: { start: 1.0, end: 0 },
            alpha: { start: 1.0, end: 0 },
            lifespan: 500,
            blendMode: Phaser.BlendModes.ADD
        };
        this.emitter = particles.createEmitter(particleConfig);


        this.particleContainer.add(particles);
        this.scene.bringToTop(this.particleContainer);

        this.emitter.startFollow(this.ball);

        this.pointParticles = this.add.particles('pixie_1');
        let pointParticleConfig = {
            speed: { min: 200, max: 360, steps: 20 },
            angle: { min: 0, max: 360 },
            scale: { start: 0.40, end: 0 },
            alpha: { start: 1.0, end: 0 },
            accelerationX: 50,
            accelerationY: 50,
            //alpha: { start: 1.0, end: 0 },
            lifespan: 1000,
            frequency: -1,
            blendMode: Phaser.BlendModes.ADD
        }
        this.pointEmitter = this.pointParticles.createEmitter(pointParticleConfig);
        this.particleContainer.add(this.pointParticles);
        this.pointEmitter.setPosition(200, 200);

    }
    initScoreData() {
        // this.otherGroupObject = this.specsObject.others;

        // this.discountPanel = new StaticObjects({
        //     scene: this,
        //     x: this.otherGroupObject.discount_panel.x * this.w,
        //     y: this.otherGroupObject.discount_panel.y * this.h,
        //     texture: this.otherGroupObject.discount_panel.texturename
        // });
        // this.discountPanel.setTint(this.colorTheme.background[1]);

        // const style = {
        //     fontSize: '20px',
        //     color: '#ffffff',
        //     align: 'left'
        // }
        // let dW = this.otherGroupObject.text_area.width * this.w;
        // let dH = this.otherGroupObject.text_area.height * this.h;

        // this.discountText = this.add.bitmapText(0, 0, 'font_export', '', 20)
        // this.elementsContainer.add(this.discountPanel);
        // this.elementsContainer.add(this.discountText);

        // this.updateText('DISCOUNT%');
        // this.discountText.alpha = 0;

    }
    initCompanyLogo()
    {
        let x = this.globalConfig.appWidth * 0.50;
        let y = this.globalConfig.appHeight * 0.25;
        // let scale = 0.50;
        this.logo = this.add.sprite(x, y, this.globalConfig.logoFilename);
        // this.logo.scale = scale;
        this.companyLogoContainer.add(this.logo);
    }
    connectPhysics() {

        this.matter.world.on('collisionend', () => {
            //console.log("collision end");
            this.isColliding = false;
        });

        this.matter.world.on('collisionactive', (e: any, bodyA: any, bodyB: any) => {

            if (bodyA == null || bodyB == null) return;
            if (bodyA.gameObject == null || bodyA.gameObject == null) return;

            //console.log("bodyA: " + bodyA.gameObject.name + " bodyB: " + bodyB.gameObject.name);
            if (bodyA.gameObject.name.indexOf('wall_panel_12') >= 0 || bodyA.gameObject.name.indexOf('wall_panel_11') >= 0 ||
                bodyA.gameObject.name.indexOf('wall_panel_5') >= 0 || bodyA.gameObject.name.indexOf('wall_panel_2') >= 0 ||
                bodyA.gameObject.name.indexOf('wall_panel_6') >= 0 || bodyA.gameObject.name.indexOf('wall_panel_14')>=0) {
                if (this.ballBody.speed < 0.7) {
                    this.isColliding = true;
                }
            }

        });

        this.matter.world.on("collisionstart", (e: any, bodyA: any, bodyB: any) => {

            if (bodyA == null || bodyB == null) return;
            if (bodyA.gameObject == null || bodyA.gameObject == null) return;

            if (this.didReachGoal > 0) return;
            //console.log("bodyA name[" + bodyA.gameObject.name + "] bodyB name[" + bodyB.gameObject.name + "]");
            if (bodyA.gameObject.name.length > 0 || bodyB.gameObject.name.length > 0) {
                let nameA = bodyA.gameObject.name;
                let nameB = bodyB.gameObject.name;

                function extractCoupon(str: String) {
                    if (str.indexOf("coupon") >= 0) {
                        //"couppon_10_a"
                        let start = str.substr(str.indexOf("_") + 1, str.length);
                        //console.log("start: " + start);
                        let end = start.substr(0, start.indexOf("_"));
                        //console.log("end: " + end);
                        let discount = parseInt(end);

                        //console.log(discount);
                        return [discount]
                    }
                    return [];
                }

                let dA = extractCoupon(nameA);
                if (dA.length > 0) {
                    if (this.didReachGoal > 0) return;
                    this.didReachGoal = 1;
                    this.goalDiscount = dA[0];
                    this.addDiscount(this.goalDiscount);
                    this.onRemoveBody(bodyA.gameObject);
                    this.pointEmitter.explode(20, this.ball.x, this.ball.y);
                    this.currentCouponName = nameA;
                    this.gameTimeout = setTimeout(() => {
                        clearInterval(this.gameTimeout);
                        //this.showOverlay();
                    }, 1000);
                    this.shootOnCan.play();
                    this.win.play();
                }
                let dB = extractCoupon(nameB);
                if (extractCoupon(nameB).length > 0) {
                    if (this.didReachGoal > 0) return;
                    this.didReachGoal = 1;
                    this.discountGoal = dB[0];
                    this.addDiscount(this.discountGoal);
                    this.onRemoveBody(bodyB.gameObject);
                    this.pointEmitter.explode(20, this.ball.x, this.ball.y);
                    this.currentCouponName = nameB;
                    this.gameTimeout = setTimeout(() => {
                        clearInterval(this.gameTimeout);
                        this.showOverlay();
                    }, 1000);

                    this.shootOnCan.play();
                    this.win.play();
                }

                if (nameA.indexOf('rotator') >= 0) {
                    //this.spawnText(this.ball.x, this.ball.y, String(this.discountRotator));
                    //this.addDiscount(this.discountRotator);
                    this.pointEmitter.explode(20, this.ball.x, this.ball.y);
                    this.rotatorHit.play();
                }
                else if (nameA.indexOf('gem_panel') >= 0) {
                    //this.spawnText(this.ball.x, this.ball.y, String(this.discountGems))
                    //this.addDiscount(this.discountGems);
                    this.pointEmitter.explode(20, this.ball.x, this.ball.y);
                    this.bonusClang.play();
                }
                else if (nameA.indexOf('wall_hitter_yellow') >= 0) {
                    //this.spawnText(this.ball.x, this.ball.y, String(this.discountYellowBar))
                    //this.addDiscount(this.discountYellowBar);
                    this.pointEmitter.explode(20, this.ball.x, this.ball.y);
                    this.bonusClang.play();
                }
                else if (nameA.indexOf('wall_hitter_red') >= 0) {
                    //this.spawnText(this.ball.x, this.ball.y, String(this.discountRedBar))
                    //this.addDiscount(this.discountRedBar);
                    this.pointEmitter.explode(20, this.ball.x, this.ball.y);
                    this.bonusClang.play();
                }

                else if (nameA.indexOf('wall_panel_14') >= 0) {
                    //this.spawnText(this.ball.x, this.ball.y, String(this.discountMovingPanel))
                    //this.addDiscount(this.discountMovingPanel);
                    this.pointEmitter.explode(20, this.ball.x, this.ball.y);
                    this.bonusClang.play();
                }
                else if (nameA.indexOf('star') >= 0) {
                    //this.spawnText(this.ball.x, this.ball.y, String(this.discountStar))
                    //this.addDiscount(this.discountStar);
                    this.onRemoveBody(bodyA.gameObject);
                    this.currentStar.destroy();
                    this.pickup.play();
                }
                else {
                    this.hit.play();
                }

            }
        });
    }

    addDiscount(value: number) {
        this.gameDiscount += value;
        if (this.gameDiscount > 15) this.gameDiscount = 15;
        this.updateText(String((this.gameDiscount).toFixed(2)) + "%");
    }

    showOverlay() {
        //console.log("on game over: " + this.overlayContainer);
        this.overlayContainer.setVisible(true);
        this.tweens.add({
            targets: this.overlayContainer,
            //scale: 1,
            alpha: 1,
            duation: 50,
            onComplete: (tween) => {

            }
        });
         this.time.delayedCall(30, () => { this.startOverlayAnimations() });
       
    }

    startOverlayAnimations() {

        let congrats = this.overlayContainer.getByName('congrats');
        let score = this.overlayContainer.getByName('score');
        //let discount = this.overlayContainer.getByName('discount');
        let lTime = 200;
        let sTime = 100;

        // this.onTweenNow(congrats, {
        //     alpha: { from: 0, to: 1 },
        //     scaleX: { from: 0, to: 1.5 },
        //     scaleY: { from: 0, to: 1.5 },
        //     ease: Phaser.Math.Easing.Linear,
        //     duration: lTime,
        //     onComplete: () => {
        //         this.onTweenNow(congrats,
        //             {
        //                 scaleX: { from: 1.5, to: 1 },
        //                 scaleY: { from: 1.5, to: 1 },
        //                 ease: Phaser.Math.Easing.Bounce
        //             }, sTime);
        //     }
        // }, lTime);

        this.currentCouponId = String(this.couponMap[this.currentCouponName].id);
        this.currentCouponIndex = this.couponMap[this.currentCouponName].index;

        this.game.events.emit(GameEvents.GAME_WON_EVENT, this.currentCouponId);

        this.time.delayedCall(31, () => { this.onDestroy() });


        // let c = this.add.sprite(0, 0, this.currentCouponName);
        // c.setTint(this.colorTheme.goalsFinished[this.currentCouponIndex]);
        // let w = c.width;
        // let h = c.height;

        // c.setPosition(this.w * 0.50, this.h * 0.50);
        // c.alpha = 0;

        // this.time.delayedCall(120, () => {
        //     this.canCountDiscount = false;
        //     this.onTweenNow(c, {
        //         alpha: { from: 0, to: 1 },
        //         scaleX: { from: 0, to: 1.5 },
        //         scaleY: { from: 0, to: 1.5 },
        //         ease: Phaser.Math.Easing.Linear,
        //         duration: lTime,
        //         onComplete: () => {
        //             this.onTweenNow(c,
        //                 {
        //                     scaleX: { from: 1.5, to: 1 },
        //                     scaleY: { from: 1.5, to: 1 },
        //                     ease: Phaser.Math.Easing.Bounce
        //                 }, sTime);

        //             this.game.events.emit(GameEvents.GAME_WON_EVENT, this.currentCouponId);
        //             this.time.delayedCall(3000, () => {
        //                 this.onDestroy();
        //             });
        //         }
        //     }, lTime);
        // });

    }

    spawnText(x: number, y: number, content: string) {
        //if(!this.scoreTextGroup) return;

        //content = "score1231";

        //const flyingScore = Phaser.GameObjects.BitmapText = this.scoreTextGroup.get(x,y,this.sKey);


        let flyingScore: Phaser.GameObjects.BitmapText = null;

        if (this.reserveTextPool.length <= 0) {
            flyingScore = this.add.bitmapText(x, y, 'font_export', content, 50);
            this.effectsContainer.add(flyingScore);
        }
        else {
            flyingScore = this.reserveTextPool.shift() as Phaser.GameObjects.BitmapText;
        }
        //console.log("FLYING SCORE: " + flyingScore);
        flyingScore.alpha = 1;
        flyingScore.text = content + "%";
        flyingScore.scaleX = flyingScore.scaleY = 1.0;
        flyingScore.x = x - flyingScore.width * 0.50;
        flyingScore.y = y - flyingScore.height * 0.50 - this.ball.height * 0.60;
        //console.log(flyingScore);


        //flyingScore.setVisible(true);
        //flyingScore.setActive(true);

        this.activeTextPool.push(flyingScore);

        this.tweens.add({
            targets: flyingScore,
            //scale: 1,
            alpha: 0,
            scaleX: { from: 1, to: 2 },
            scaleY: { from: 1, to: 2 },
            duation: 1500,
            onComplete: (tween) => {
                //this.scoreTextGroup!.killAndHide(flyingScore);
                //this.tweens.killTweensOf(flyingScore);
                //var id = this.activeTextPool.indexOf(targets);
                //console.log("onComplete id: " + id);

                let fs: any = this.activeTextPool.shift();
                //console.log(fs);
                //(fs as Phaser.GameObjects.BitmapText).visible = false;
                this.reserveTextPool.push(fs as Phaser.GameObjects.BitmapText);
                this.tweens.killTweensOf(fs);
            },
            onUpdate: () => {
                flyingScore.y -= 0.92;
            }
        });

        return flyingScore;
    }
    onClick() {
        //console.log("clicked me in gamescene");

        // if (this.pointEmitter != null) {
        //     this.pointEmitter.setPosition(200, 200);


        //     console.log("should explode!");
        // }



        if (this.releaseBall > 0) return;


        this.drone.play({ key: 'drone_release_', repeat: 0 });
        this.ballShapeObject = this.shapesObject[this.lifterGroupObject.ball.texturename];
        this.matter.add.gameObject(this.ball, { label: "ball", chamfer: 0, density: 2, restitution: 1.5, friction: 0.05, x: this.ball.x, y: this.ball.y, isStatic: false, shape: this.ballShapeObject });
        this.releaseBall = 1;

        this.ballBody = this.ball.body as MatterJS.BodyType;

        this.initParticles();

    }
    onHitObject(e: any, bodyA: any, bodyB: any) {


        //console.log("event[" + e + "] bodyA name[" + bodyA.gameObject.name + "] bodyB name[" + bodyB.gameObject.name + "]");
        //console.log(e);

        if (bodyA.gameObject.name.length > 0 || bodyB.gameObject.name.length > 0) {
            let nameA = bodyA.gameObject.name;
            let nameB = bodyB.gameObject.name;

            function extractCoupon(str: String) {
                if (str.indexOf("coupon") >= 0) {
                    let start = str.substr(str.indexOf("_") + 1, str.length);
                    //console.log("start: " + start);
                    let end = start.substr(0, start.indexOf("_"));
                    //console.log("end: " + end);
                    let discount = parseInt(end);
                    //console.log(discount);
                    return [discount]
                }
                return [];
            }

            let dA = extractCoupon(nameA);
            if (dA.length > 0) {
                this.gameDiscount += dA[0];
                this.onRemoveBody(bodyA.gameObject);
            }
            let dB = extractCoupon(nameB);
            if (extractCoupon(nameB).length > 0) {
                this.gameDiscount += dB[0];
                this.onRemoveBody(bodyB.gameObject);
            }
        }
        this.onCheck();
    }
    updateText(value: string) {
        // this.discountText.text = value;
        // this.discountText.x = this.discountPanel.x - (this.discountText.width * 0.50);
        // this.discountText.y = this.discountPanel.y;// - (this.discountText.height*0.050);

    }
    onCheck() {
        //console.log("Adsfasdfasd");
    }
    onTweenNow(target: any, property: any, duration: integer) {
        let obj: any = {};
        obj.targets = target;

        for (let s in property) {
            //console.log(s + " " + property[s]);
            obj[s] = property[s];
        }
        obj.duration = duration;

        this.tweens.add(obj);

    }
    onRemoveBody(e: any) {


        let body = e.body as MatterJS.BodyType;
        this.matter.world.remove(body);

        //FOR GOALS
        for (let i = 0; i < this.goals.length; i++) {
            //console.log(this.goals[i].name);
            if (this.goals[i] == e) {
                let target = this.finishedCoupons[i];
                this.onTweenNow(this.goals[i], { alpha: { from: 0.80, to: 0, scaleX: 0.50, scaleY: 0.50 }, y: '-=700', ease: Phaser.Math.Easing.Linear }, 1500);
                this.onTweenNow(target, { alpha: { from: 0, to: 1, scaleX: 0.50, scaleY: 0.50 }, ease: Phaser.Math.Easing.Bounce }, 100);
                this.onTweenNow(target,
                    {
                        alpha: { from: 0, to: 1 },
                        scaleX: { from: 0.50, to: 0.60 },
                        scaleY: { from: 0.50, to: 0.60 },
                        ease: Phaser.Math.Easing.Linear,
                        onComplete: () => {
                            this.onTweenNow(target,
                                {
                                    scaleX: { from: 0.60, to: 0.50 },
                                    scaleY: { from: 0.60, to: 0.50 },
                                    ease: Phaser.Math.Easing.Bounce
                                }, 70);
                        }
                    }, 30);

            }
            else {
                this.goals[i].alpha = 0.50;
                let target = this.finishedCoupons[i];
                this.tweens.addCounter({
                    from: 0,
                    to: 128,
                    duration: 30,
                    onUpdate: (tween) => {
                        const value = Math.floor(tween.getValue());

                        this.goals[i].setTint(Phaser.Display.Color.GetColor(value, value, value));
                    }
                });

            }
        }
    }

    update() {

        if (this.releaseBall == 0) {
            this.ball.x = this.drone.x;
        }
        else {
            if (this.didReachGoal != 1) {
                //console.log("ball velocity: x[" + this.ball.body.velocity.x + "] y["+this.ball.body.velocity.x + "]");

                if (this.isColliding) {
                    if (this.ballBody.speed < 0.7) {
                        this.isColliding = false;
                        this.matter.applyForce(this.ballBody,
                            {
                                x: Math.random() * 0.20 - Math.random() * 0.20,
                                y: 0
                            });
                    }
                }

            }

        }

        if (this.drone != null) {
            if (this.releaseBall <= 0) this.drone.update();
        }

        //NOTE: Ommitted temporarily since there is no discount counting
        // if (this.canCountDiscount) {
        //     this.finalCount += 0.25;
        //     this.coin.play();

        //     if (this.finalCount >= 15) {
        //         this.finalCount = 15;
        //         this.canCountDiscount = false;
        //         this.finalDiscountText.text = String(Math.floor(this.finalCount) + "%");
        //         this.game.events.emit(GAME_WON_EVENT, 'MyCouponID')
        //         setTimeout(() => {
        //             this.onDestroy();
        //         }, 2000);
        //     }
        //     else {
        //         if (this.finalCount >= this.gameDiscount) {
        //             this.finalCount = this.gameDiscount;
        //             this.canCountDiscount = false;
        //             this.finalDiscountText.text = String((this.finalCount).toFixed(2) + "%");
        //             this.game.events.emit(GAME_WON_EVENT, 'MyCouponID')
        //             setTimeout(() => {
        //                 this.onDestroy();
        //             }, 2000);
        //         }
        //         this.finalDiscountText.text = String((this.finalCount).toFixed(2) + "%");
        //     }

        // }

        if (this.movingList.length > 0) {
            for (let i = 0; i < this.movingList.length; i++) {
                this.movingList[i].update();
            }
        }
    }

    onDestroy() {
       
        // var currentGame: PhaserGame = this.game as PhaserGame;
        // let localGame = currentGame.localGame;
        // localGame.onGameEnd();

        this.game.events.emit(GameEvents.GAME_OVER, "");
        // this.game.destroy(true);
        // this.sys.game.destroy(true);

        // var options: IGameOptions = currentGame.localGame.gameOptions as IGameOptions;

        // this.game.destroy(true);

        // let phaserGame = new PhaserGame(GameConfig);

        // let nextGame = new Game(phaserGame);
        // nextGame.gameOptions = options;
        // phaserGame.setReference(nextGame);
        // phaserGame.name = "wiply";

    }
}
