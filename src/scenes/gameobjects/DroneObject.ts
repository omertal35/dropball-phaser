import { WiplySpriteInterface } from "../../interfaces/wiplysprite.interface";
import { StaticObjects } from "./StaticObjects";

export class DroneObject extends StaticObjects{

    //body: Phaser.Types.Physics.Matter.MatterBody;
    // local_color: string;
    // gameObjectType:integer;
    // kineticMovementType:integer;
    // kineticSubMovementType:integer;
    // localRotation:number = 0;

    canMove:boolean = false;
    maxDistance:number = 250;
    direction:integer = 0;
    origX:number = 0;
    speed:number = 2;
    leftBounds:number = 0;
    rightBounds:number = 0;

    constructor(aParams: WiplySpriteInterface) {

        super(aParams);

        
    }

    protected initSprite() {

       
    }

    protected initPhysics() {
        console.log("init physics drone")
    }

    initialize()
    {
        
        this.origX = this.x;
        this.leftBounds = this.origX - this.maxDistance;
        this.rightBounds = this.origX + this.maxDistance;
        this.direction = 1;
        this.canMove = true;
        console.log("initSprite " + this.canMove);
    }
    public update() {

        //console.log("distance: " + this.canMove);
        if(this.canMove)
        {
           
            //console.log("distance: " + distance);

            if(this.direction>0)
            {
                let distance = Math.abs(this.x-this.origX);
                this.x += this.speed;
                if(this.x > this.rightBounds)
                {
                    this.x = this.origX + this.maxDistance;
                    this.direction = -1;
                }
            }
            else
            {
                let distance = Math.abs(this.x-this.origX);
                this.x -= this.speed;
                if(this.x<this.leftBounds)
                {
                    this.x = this.origX - this.maxDistance;
                    this.direction = 1;
                }
            }
        }
       
    }


}