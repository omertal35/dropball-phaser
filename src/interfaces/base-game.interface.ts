export interface IWorldColor {
    background: Array<any>,
    bounds: any,
    obstacles: 0xffffff,
    rotators: Array<any>,
    leftScoreWall: any,
    rightScoreWall: any,
    movingPlatform: any,
    hero: any,
    pickups: any,
    lifter: any,
    goals: Array<any>,
    diamonds: Array<any>,
    goalsFinished: Array<any>,
    gameTitle: any
}


export interface IGameCoupon {
    id: string;
    imgUrl?: string,
}

export interface IGameOptions {
    colors?: string[],
    coupons?: IGameCoupon[],
    logoImgUrl?: string,
    bgImgUrl?: string,
    isEditMode?: boolean
}

export abstract class BaseGame {

    protected isGameDisabled: boolean;

    public setColors(colors: string[]) { }
    public setLogos(urls: string[]) { }
    public setCoupons(coupons: IGameCoupon[]) { }

    public disableGame = () => this.isGameDisabled = true
    public enableGame = () => this.isGameDisabled = false

    public onGameLoaded() { };
    public onGameLost() { };
    public onGameEnd() { };
    public onGameWon(couponId: string) { };

    public retryGame() { };
    public dispose?() { };

    public check() { };

}
