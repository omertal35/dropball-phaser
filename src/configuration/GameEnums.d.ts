export enum GameObjectType {
    Bounds,
    InteractiveBounds,
    Pickup,
    Enemy
}

export enum KineticMovementType {
    Steady,
    Horizontal,
    Vertical,
    Diagonal,
}

export enum KineticSubMovementType
{
    Steady,
    Rotating,
    Jumping
}