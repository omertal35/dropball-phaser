import { BootScene } from './scenes/BootScene';
import { GameScene } from './scenes/GameScene';
import { MainScene } from './scenes/main-scene';
import { TitleScene } from './scenes/TitleScene';

export const GameConfig: Phaser.Types.Core.GameConfig = {
  //title: 'Drop Ball',
  url: 'https://github.com/digitsensitive/phaser3-typescript',
  version: '2.0',
  width: 720,
  height: 1240,
  backgroundColor: 0xffffff,
  type: Phaser.AUTO,
  parent: 'game',
  transparent: true,
  physics: {
    default: 'matter',
    
    matter: {
      debug: {
        showBody: false,
        showStaticBody: false
      },
      //showBody: true,
      //showStaticBody: true,
      enableSleeping: true,
      gravity: { y: 2 }
    }
  },
  scale: {
    mode: Phaser.Scale.FIT,
    autoCenter: Phaser.Scale.CENTER_BOTH,
  },
  scene: [MainScene, BootScene, TitleScene, GameScene]
};
